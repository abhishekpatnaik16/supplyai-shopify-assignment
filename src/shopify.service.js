import Axios from 'axios';

export const fetchProducts = (page = 1, limit = 5) => {
  return Axios.get(
    `/shopify/products.json?limit=${limit}&page=${page}&fields=id,images,title,variants,body_html,images`,
    {
      auth: {
        username: 'd891ae47080fe65dc23df0eb755e22ef',
        password: '0aef9c360cb2541ef92d259b8e5838f8'
      }
    }
  ).then(response => response.data.products);
};
