import React, { Component, Fragment } from 'react';
import ShopNavBar from '../../components/ShopNavBar';
import ProductsWrapper from '../../components/ProductsWrapper';

export default class Home extends Component {
  render() {
    return (
      <Fragment>
        <ShopNavBar />
        <ProductsWrapper />
      </Fragment>
    );
  }
}
