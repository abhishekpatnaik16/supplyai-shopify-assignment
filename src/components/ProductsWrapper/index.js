import React, { Component } from 'react';
import FetchProgress from '../FetchProgress';
import { Grid, Row, Col } from 'react-bootstrap';
import Product from '../Product';
import { once } from 'lodash';
import { fetchProducts } from '../../shopify.service';

export default class ProductsWrapper extends Component {
  state = {
    page: 1,
    perPage: 10,
    allProductsExhausted: false,
    products: [],
    fetchInProgress: false,
    showRetry: false
  };

  componentDidMount() {
    window.addEventListener('scroll', this.scrollHandler);
    this.fetchMoreProductsOnce = once(fetchProducts);
    this.startProductsFetch();
  }

  scrollHandler = event => {
    if (this.state.allProductsExhausted) return;
    const lastProduct = document.querySelector(
      'div.products-wrapper > div.product:last-child'
    );
    const lastProductRect = lastProduct.getBoundingClientRect();
    const offset = 100;
    if (lastProductRect.top <= window.innerHeight + offset) {
      this.startProductsFetch();
    }
  };

  startProductsFetch = async () => {
    if (this.state.fetchInProgress) return;
    this.setState(
      {
        fetchInProgress: true
      },
      () => {
        // Start fetch
        this.fetchMoreProductsOnce(this.state.page + 1, this.state.perPage)
          .then(data => {
            // Fetch Complete
            if (data.length === 0) {
              this.setState({
                allProductsExhausted: true,
                fetchInProgress: false
              });
            } else {
              this.setState(
                prevState => {
                  return {
                    products: prevState.products.concat(data),
                    page: prevState.page + 1,
                    fetchInProgress: false,
                    showRetry: false
                  };
                },
                () => {
                  this.fetchMoreProductsOnce = once(fetchProducts);
                }
              );
            }
          })
          .catch(err => {
            this.setState({
              showRetry: true,
              fetchInProgress: false
            });
            this.fetchMoreProductsOnce = once(fetchProducts);
          });
      }
    );
  };

  showFetchProgress = () => {
    if (this.state.showRetry) {
      return (
        <FetchProgress retry={true} retryHandler={this.startProductsFetch} />
      );
    }
    if (this.state.fetchInProgress) {
      return <FetchProgress />;
    }
  };

  render() {
    return (
      <Grid>
        <Row>
          <Col xs={12} className="products-wrapper">
            {this.state.products.map(data => {
              return <Product data={data} key={data.id} />;
            })}
          </Col>
        </Row>
        <Row>
          <Col xs={12}>{this.showFetchProgress()}</Col>
        </Row>
      </Grid>
    );
  }
}
