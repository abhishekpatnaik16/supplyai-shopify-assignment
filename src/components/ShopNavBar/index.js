import React from 'react';
import { Navbar, Image } from 'react-bootstrap';

export default () => {
  return (
    <Navbar
      style={{
        backgroundColor: '#0245BE'
      }}
    >
      <Navbar.Header>
        <Navbar.Brand>
          <Image src={'/assets/images/logo.png'} />
        </Navbar.Brand>
      </Navbar.Header>
    </Navbar>
  );
};
