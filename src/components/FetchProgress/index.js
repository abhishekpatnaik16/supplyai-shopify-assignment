import React from 'react';
import { ProgressBar } from "react-bootstrap";

export default ({ retry, retryHandler }) => {
  if(retry) {
    return (
      <div>
        Failed to fetch results. <a onClick={retryHandler}>Retry</a>
      </div>
    )
  } else {
    return (
      <div>
        <ProgressBar active now={100}/>
      </div>
    );
  }
}