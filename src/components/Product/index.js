import React, { Component } from 'react';
import { Row, Col, Image, Grid, Label, Glyphicon } from 'react-bootstrap';
import './style.css';
import { sortBy } from 'lodash';

export default class Product extends Component {
  state = {
    showVariants: false
  };

  static getDerivedStateFromProps(props, state) {
    const newState = {
      ...state,
      ...props.data
    };
    newState.variants = sortBy(newState.variants, ['price']);

    return newState;
  }

  toggleVariantsCollapsible = () => {
    this.setState(prevState => {
      return {
        showVariants: !prevState.showVariants
      };
    });
  };

  getPriceRangeString = () => {
    const { variants } = this.state;
    if (variants[0].price === variants[variants.length - 1].price)
      return `$${variants[0].price}`;
    return `$${variants[0].price} - $${variants[variants.length - 1].price}`;
  };

  getImage = () => {
    const { images } = this.state;
    if (images[0]) {
      return images[0].src;
    } else {
      return '/assets/images/no-image-thumb.jpg';
    }
  };

  toggleVariantList = () => {
    this.setState(prevState => {
      return {
        showVariants: !prevState.showVariants
      };
    });
  };

  renderVariants = () => {
    const { title, variants } = this.state;
    return (
      <Row>
        <Col xs={12}>
          <table
            className={[
              'variant-list',
              this.state.showVariants ? 'expand' : ''
            ].join(' ')}
          >
            <tbody>
              {variants.map(variant => (
                <tr key={variant.id}>
                  <td className="variant-title">{`${title} - ${
                    variant.title
                  }`}</td>
                  <td className="variant-price">{variant.price}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </Col>
      </Row>
    );
  };

  render() {
    const { title, body_html, variants } = this.state;
    return (
      <Grid className="product" onClick={this.toggleCollapsible}>
        <Row>
          <Col xs={4} sm={3}>
            <Image src={this.getImage()} thumbnail />
          </Col>
          <Col xs={8} sm={9}>
            <p className="product-name">{title}</p>
            <p className="product-price">{this.getPriceRangeString()}</p>
            <p className="product-desc hidden-xs hidden-sm">{body_html}</p>
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <hr style={{ marginTop: 10, marginBottom: 10 }} />
          </Col>
        </Row>
        <Row onClick={this.toggleVariantsCollapsible}>
          <Col xs={12}>
            <span className="variants-count">
              <Label className="variants-label">{variants.length}</Label>{' '}
              variant(s) for this product
            </span>
            <Glyphicon glyph={(this.state.showVariants) ? 'triangle-top' : 'triangle-bottom'} className="collapsible-arrow" />
          </Col>
        </Row>
        {this.renderVariants()}
      </Grid>
    );
  }
}
